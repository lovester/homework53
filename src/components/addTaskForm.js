import React, {Component} from "react"

const AddTaskForm = props => (
    <div className="addTaskForm">
        <input type="text" className="taskText"
               onChange={props.changeText}
               value={props.value}/>
        <button className="addTask" onClick={props.addNewTask}>Add</button>
    </div>
);

export default AddTaskForm;