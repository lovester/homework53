import React, {Component} from "react"

const Task = props => (
    props.allTasks.map((task, id) => {
        return (
            <div key={id} className="task">
                <input type="checkbox" className="check"/>
                <p className="taskText">
                    {task.text}
                    <button className="delete" onClick={() => props.deleteTask(task.id)}>x</button>
                </p>
            </div>
        )
    })
);

export default Task;