import React, {Component, Fragment} from 'react';

import './App.css';
import AddTaskForm from "./components/addTaskForm";
import Task from "./components/task";


class App extends Component {
    state = {
        tasks: [
            {text: 'Do homework', id: '1'},
            {text: 'Go sleep', id: '2'}
        ],
        taskText: '',
        taskId: 2
    };

    changeHandler = (event) => {
        this.setState({
            taskText: event.target.value
        })
    };

    addNewTask = () => {
        let tasks = [...this.state.tasks];
        const newTask = {
            text: this.state.taskText,
            id: this.state.taskId + 1
        };
        tasks.push(newTask);
        this.setState({
            tasks: tasks,
            taskText: '',
            taskId: this.state.taskId + 1
        })
    };

    deleteTask = (id) => {
        let tasks = [...this.state.tasks];
        const index = tasks.findIndex(task => task.id === id);
        tasks.splice(index, 1);

        this.setState({
            tasks: tasks,
            taskText: '',
            taskId: this.state.taskId - 1
        });
    };

    render() {
        return (
            <Fragment>
                <AddTaskForm
                    changeText={(event) => this.changeHandler(event)}
                    addNewTask={() => this.addNewTask()}
                    value={this.state.taskText}
                />
                <Task
                    allTasks={this.state.tasks}
                    deleteTask={(id) => this.deleteTask(id)}
                />
            </Fragment>
        )
    }
}

export default App;
